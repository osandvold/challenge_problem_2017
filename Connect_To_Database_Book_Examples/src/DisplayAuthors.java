import java.sql.*;

/** this class is trying to display information from the database*/
public class DisplayAuthors {

    static final String DATABASE_URL = "jdbc:mysql:?//localhost/books";

    public static void main(String[] args){
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try{
            connection = DriverManager.getConnection(DATABASE_URL,"deitel","deitel");

            statement = connection.createStatement();

            resultSet = statement.executeQuery("SELECT AuthorID, LastName, FirstName FROM Authors");

            ResultSetMetaData metaData = resultSet.getMetaData();
            int numCols = metaData.getColumnCount();

            System.out.println("Authors from Authors Table: \n");

            for(int i =1;i<=numCols;i++){
                System.out.printf("%-8s\t",metaData.getColumnName(i));
            }
            System.out.println();

            while(resultSet.next()){
                for(int i=0;i<=numCols;i++){
                    System.out.printf("%-8s\t",resultSet.getObject(i));
                }
                System.out.println();
            }

        }catch (SQLException err){
            err.printStackTrace();
        }finally{
            try{
                resultSet.close();
                statement.close();
                connection.close();
            }catch(Exception exception){
                exception.printStackTrace();
            }
        }
    }

}
