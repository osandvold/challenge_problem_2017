Olivia Sandvold
Chris Sullivan
Anjela

This program is a chat server application for Android devices with KitKat 4.4 and API 19 or higher.
The application server has access to a database from which it can authenticate user login credentials 
or create a new user and store in the database. Users can connect to each other or add themselves to a group with more
than one other user. 